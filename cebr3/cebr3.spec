#include "spec/spec.spec"
#include "spec/land_std_vme.spec"

SUBEVENT(CEBR3_VME){
    land_vme = LAND_STD_VME();
    select several {
	    // mdpp[0]=VME_MESYTEC_MDPP32(geom=0);
	    mdpp[0]=VME_MESYTEC_MDPP32_FREE(geom=0);
    }
}

EVENT
{
  vme = CEBR3_VME(type=10,subtype=1);
}

// This enforces zero suppression at strip level

// SIGNAL(ZERO_SUPPRESS_MULTI(20000): SHORT1);
SIGNAL(ZERO_SUPPRESS_MULTI(20000): LONG1);
SIGNAL(ZERO_SUPPRESS_MULTI(20000): TRG1);

// ADC channels

// SIGNAL(SHORT1,      vme.mdpp[0].adc_short[ 0],
//        SHORT32,     vme.mdpp[0].adc_short[31], DATA16_OVERFLOW);
// SIGNAL(LONG1,       vme.mdpp[0].adc[ 0],
//        LONG32,      vme.mdpp[0].adc[31], DATA16_OVERFLOW);
SIGNAL(LONG1,       vme.mdpp[0].adc[ 0],
       LONG32,      vme.mdpp[0].adc[31], DATA16_OVERFLOW);
SIGNAL(TRG1, vme.mdpp[0].trigt[0],
       TRG2, vme.mdpp[0].trigt[1], DATA32);
