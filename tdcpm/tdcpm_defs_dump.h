/* This file is part of UCESB - a tool for data unpacking and processing.
 *
 * Copyright (C) 2024  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __TDCPM_DEFS_DUMP_H__
#define __TDCPM_DEFS_DUMP_H__

#include "tdcpm_units.h"

/**************************************************************************/

size_t tdcpm_dump_unit(tdcpm_unit_index unit_idx);
size_t tdcpm_dump_tspec(tdcpm_tspec_index tspec_idx);

/**************************************************************************/

#endif/*__TDCPM_DEFS_STRUCT_H__*/
