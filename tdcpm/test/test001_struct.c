/* This file is part of UCESB - a tool for data unpacking and processing.
 *
 * Copyright (C) 2022  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "tdcpm_struct_info.h"

TDCPM_STRUCT_DEF
typedef struct calt_t
{
  double _k TDCPM_UNIT("ns/ch");
  double _m TDCPM_UNIT("ns");
  uint32_t _i;
} TDCPM_STRUCT_ALIGN_8 calt;

TDCPM_STRUCT_DEF
typedef struct cale_t
{
  double _k TDCPM_UNIT("10 MeV/ch");
  double _m TDCPM_UNIT("10 MeV");
} cale;

#define SEVEN 7  /* Test with a macro. */

TDCPM_STRUCT_DEF
typedef struct cal2_t
{
  double _a;
  double _b[SEVEN];
  double _c [ 5 ][ 6 ] ;  /* Intentional spaces for parser test. */

  calt   _d;
  cale   _e[4];
  cale   _f[2][3];

  double _g[9] TDCPM_UNIT("cm");
} cal2;

TDCPM_STRUCT_DEF
typedef struct calu_t
{
  const char *_str;
  const char *_ident;
} calu;

TDCPM_STRUCT_INST(r) cal2 _cal_det_r;
TDCPM_STRUCT_INST(s) calt _cal_det_s[7];
TDCPM_STRUCT_INST(t) cal2 _cal_det_t[7][6];
TDCPM_STRUCT_INST(u) calu _cal_det_u[5];

/* Auto-generated structure info setup: */

#include "test/generated/test001_struct_decl.c"

/* The below function tests _manual_ setup of the structure
 * information.  By using the code in the automatically generated file
 * above, it is NOT needed in a user application.
 *
 * It exists here for testing.
 */

#include "test001_struct_manual.c"
