// -*- C++ -*-

/* This file is part of UCESB - a tool for data unpacking and processing.
 *
 * Copyright (C) 2024  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

/* The kind of items we find:
 *
 * Febex block:
 *
 * EBYE_FEBEX_HIT:     info-7     info-8     febex-0    febex-2    febex-3
 *                     ts47..28   ts63..48   data[0]    data[2]    data[3]
 *
 * ASIC block:
 * All second-word timestamps marked with 1 in bit 28)
 *
 * EBYE_SYNC100:       info-5     info-4
 *                     ts63..48   ts47..28
 * EBYE_INFO_14:       info-5     info-14
 *                     ts63..48   ts47..28
 * EBYE_INFO_15:       info-5     info-15
 *                     ts63..48   ts47..28
 * EBYE_R3B_ASIC_HIT:  info-5     info-7    asic
 *                     ts63..48   ts47..28  d
 *
 * CAEN block:
 *
 * EBYE_CAEN_HIT:      info-4     caen-0     caen-1     caen-3
 *                     ts47..28   data[0]    data[1]    data[3]
 */

EBYE_INFO_ITEM(code, ts_mark=0)
{
  UINT32 w0
    {
      0_19:  field;
      20_23: code     = MATCH(code);
      24_29: module;
      30_31: type     = 0b10;
    }

  UINT32 w1
    {
      0_27:  ts;
      28_31: ts_mark  = MATCH(ts_mark);
    }

  MATCH_END;
}

/* This is a duplicate of EBYE_INFO_ITEM just to be able to check the
 * timestamp.  Would be nice to be able to give a wildcard, in which
 * case matching of that member (in the case above) is omitted.
 */
EBYE_INFO_ITEM_TS(code,ts)
{
  UINT32 w0
    {
      0_19:  field;
      20_23: code     = MATCH(code);
      24_29: module;
      30_31: type     = 0b10;
    }

  UINT32 w1
    {
      0_27:  ts       = MATCH(ts);
      28_31: 0;
    }

  MATCH_END;
}

EBYE_FEBEX_ITEM(sfp, board, data_idx)
{
  UINT32 w0
    {
      0_15:  adc_data;
      16_19: ch;
      20_21: data_idx = MATCH(data_idx);
      22_25: board    = MATCH(board);
      26_27: sfp      = MATCH(sfp);
      28:    clip;
      29:    pileup;
      30_31: type     = 0b11;
    }

  UINT32 w1
    {
      0_27:  ts;
      28_31: 0;
    }
}

/* This is a duplicate of EBYE_FEBEX_ITEM just to be able to check the
 * channel.  Would be nice to be able to give a wildcard, in which
 * case matching of that item (in the case above) is omitted.
 */
EBYE_FEBEX_ITEM_CH(sfp, board, data_idx, ch, ts)
{
  UINT32 w0
    {
      0_15:  adc_data;
      16_19: ch       = MATCH(ch);
      20_21: data_idx = MATCH(data_idx);
      22_25: board    = MATCH(board);
      26_27: sfp      = MATCH(sfp);
      28:    clip;
      29:    pileup;
      30_31: type     = 0b11;
    }

  UINT32 w1
    {
      0_27:  ts       = MATCH(ts);
      28_31: 0;
    }
}

EBYE_FEBEX_HIT(sfp,board)
{
  /* Worst case: 1638 items for same channel in one record.
   * (0x10000 = 65536) / (5 * 2 * 4) = 1638.
   */

  MEMBER(DATA16_OVERFLOW data0[16] ZERO_SUPPRESS_MULTI(1638));
  MEMBER(DATA16_OVERFLOW data2[16] ZERO_SUPPRESS_MULTI(1638));
  MEMBER(DATA16_OVERFLOW data3[16] ZERO_SUPPRESS_MULTI(1638));
  MEMBER(DATA32          ts_lo[16] ZERO_SUPPRESS_MULTI(1638));
  MEMBER(DATA32          ts_hi[16] ZERO_SUPPRESS_MULTI(1638));

  /* TODO: cannot check ts in info2 or item0, since generated match
   * code cannot access the info1.w1.ts member.
   *
   * Implement an CHECK/ASSERT function.
   */
  info7 = EBYE_INFO_ITEM    (code = 7);                    /* ts 47..28 */
  info8 = EBYE_INFO_ITEM    (code = 8/*,ts=info1.w1.ts*/); /* ts 63..48 */
  item0 = EBYE_FEBEX_ITEM   (sfp=sfp,board=board, data_idx=0);
  MATCH_END;
  item2 = EBYE_FEBEX_ITEM_CH(sfp=sfp,board=board, data_idx=2,
			     ch=item0.w0.ch, ts=info7.w1.ts);
  item3 = EBYE_FEBEX_ITEM_CH(sfp=sfp,board=board, data_idx=3,
			     ch=item0.w0.ch, ts=info7.w1.ts);

  ENCODE(data0[item0.w0.ch], (value=item0.w0.adc_data));
  ENCODE(data2[item0.w0.ch], (value=item2.w0.adc_data));
  ENCODE(data3[item0.w0.ch], (value=item3.w0.adc_data));

  /* Timestamp: 63..48  47..28  27..0 */

  ENCODE(ts_lo[item0.w0.ch], (value=(item0.w1.ts |
				     ((info7.w0.field & 0xf) << 28))));
  ENCODE(ts_hi[item0.w0.ch], (value=(((info7.w0.field      ) >>  4) |
				     ((info8.w0.field      ) << 16))));
}

EBYE_SYNC100()
{
  info5 = EBYE_INFO_ITEM    (code = 5, ts_mark=1); /* ts 63..48 */
  info4 = EBYE_INFO_ITEM    (code = 4, ts_mark=1
			     /*,ts=info1.w1.ts*/); /* ts 47..28 */
  MATCH_END;
}

EBYE_INFO_15()
{
  info5 = EBYE_INFO_ITEM    (code = 5, ts_mark=1); /* ts 63..48 */
  info4 = EBYE_INFO_ITEM    (code =15, ts_mark=1
			     /*,ts=info1.w1.ts*/); /* ?? */
  MATCH_END;
}

EBYE_INFO_14()
{
  info5 = EBYE_INFO_ITEM    (code = 5, ts_mark=1); /* ts 63..48 */
  info4 = EBYE_INFO_ITEM    (code =14, ts_mark=1
			     /*,ts=info1.w1.ts*/); /* ?? */
  MATCH_END;
}

EBYE_R3B_ASIC_ITEM(sfp=0, board=0, ts_mark=0)
{
  UINT32 w0
    {
      0_11:  adc_data;
      12_18: ch;
      19_22: asic;
      23_28: module;
      29:    pileup;
      30_31: type     = 0b11;
    }

  UINT32 w1
    {
      0_27:  ts;
      28_31: ts_mark  = MATCH(ts_mark);
    }
}

EBYE_R3B_ASIC_HIT(sfp=0,board=0)
{
  info5 = EBYE_INFO_ITEM    (code = 5, ts_mark=1); /* ts 63..48 */
  info7 = EBYE_INFO_ITEM    (code = 7, ts_mark=1
			     /*,ts=info1.w1.ts*/); /* ts 47..28 */
  MATCH_END;
  item0 = EBYE_R3B_ASIC_ITEM(sfp=sfp,board=board,
			     ts_mark=1);

}

EBYE_CAEN_ITEM(module, data_idx)
{
  UINT32 w0
    {
      0_15:  adc_data;
      16_21: ch;
      22_23: data_idx = MATCH(data_idx);
      24_28: module   = MATCH(module);
      29:    0;
      30_31: type     = 0b11;
    }

  UINT32 w1
    {
      0_27:  ts;
      28_31: 0;
    }
}

/* This is a duplicate of EBYE_CAEN_ITEM just to be able to check the
 * channel.  Would be nice to be able to give a wildcard, in which
 * case matching of that item (in the case above) is omitted.
 */
EBYE_CAEN_ITEM_CH(module, data_idx, ch, ts)
{
  UINT32 w0
    {
      0_15:  adc_data;
      16_21: ch       = MATCH(ch);
      22_23: data_idx = MATCH(data_idx);
      24_28: module   = MATCH(module);
      29:    0;
      30_31: type     = 0b11;
    }

  UINT32 w1
    {
      0_27:  ts       = MATCH(ts);
      28_31: 0;
    }
}

EBYE_CAEN_HIT(module)
{
  /* Worst case: 1638 items for same channel in one record.
   * (0x10000 = 65536) / (4 * 2 * 4) = 2048.
   */

  MEMBER(DATA16_OVERFLOW data0[16] ZERO_SUPPRESS_MULTI(2048));
  MEMBER(DATA16_OVERFLOW data1[16] ZERO_SUPPRESS_MULTI(2048));
  MEMBER(DATA16_OVERFLOW data3[16] ZERO_SUPPRESS_MULTI(2048));
  MEMBER(DATA32          ts_lo[16] ZERO_SUPPRESS_MULTI(2048));
  MEMBER(DATA32          ts_hi[16] ZERO_SUPPRESS_MULTI(2048));

  /* TODO: cannot check ts in info2 or item0, since generated match
   * code cannot access the info1.w1.ts member.
   *
   * Implement an CHECK/ASSERT function.
   */
  info4 = EBYE_INFO_ITEM    (code = 4);                    /* ts 47..28 */
  item0 = EBYE_CAEN_ITEM    (module=module, data_idx=0);
  MATCH_END;
  item1 = EBYE_CAEN_ITEM_CH (module=module, data_idx=1,
			     ch=item0.w0.ch, ts=info4.w1.ts);
  item3 = EBYE_CAEN_ITEM_CH (module=module, data_idx=3,
			     ch=item0.w0.ch, ts=info4.w1.ts);

  ENCODE(data0[item0.w0.ch], (value=item0.w0.adc_data));
  ENCODE(data1[item0.w0.ch], (value=item1.w0.adc_data));
  ENCODE(data3[item0.w0.ch], (value=item3.w0.adc_data));

  /* Timestamp: 63..48  47..28  27..0 */

  ENCODE(ts_lo[item0.w0.ch], (value=(item0.w1.ts |
				     ((info4.w0.field & 0xf) << 28))));
  ENCODE(ts_hi[item0.w0.ch], (value=(((info4.w0.field      ) >>  4)/* |*/
				   /*((info8.w0.field      ) << 16)*/)));

}


SUBEVENT(RECORD_EVENT)
{
  select several
    {
      /* This is not great:
       *
       * The matching of each EBYE_FEBEX_HIT will try all of them,
       * which means that for each hit, two info items are just passed
       * and then the match is done on the first hit word...
       *
       * Performant?  Not really.
       *
       * TODO: deep optimised matching.
       */

      /* TODO: two-dimensional array of items, i.e. febex[][] = ... */
      febex0[ 0] = EBYE_FEBEX_HIT(sfp=0,board= 0);
      febex0[ 1] = EBYE_FEBEX_HIT(sfp=0,board= 1);
      febex0[ 2] = EBYE_FEBEX_HIT(sfp=0,board= 2);
      febex0[ 3] = EBYE_FEBEX_HIT(sfp=0,board= 3);
      febex0[ 4] = EBYE_FEBEX_HIT(sfp=0,board= 4);
      febex0[ 5] = EBYE_FEBEX_HIT(sfp=0,board= 5);
      febex0[ 6] = EBYE_FEBEX_HIT(sfp=0,board= 6);
      febex0[ 7] = EBYE_FEBEX_HIT(sfp=0,board= 7);
      febex0[ 8] = EBYE_FEBEX_HIT(sfp=0,board= 8);
      febex0[ 9] = EBYE_FEBEX_HIT(sfp=0,board= 9);
      febex0[10] = EBYE_FEBEX_HIT(sfp=0,board=10);
      febex0[11] = EBYE_FEBEX_HIT(sfp=0,board=11);
      febex0[12] = EBYE_FEBEX_HIT(sfp=0,board=12);
      febex0[13] = EBYE_FEBEX_HIT(sfp=0,board=13);
      febex0[14] = EBYE_FEBEX_HIT(sfp=0,board=14);
      febex0[15] = EBYE_FEBEX_HIT(sfp=0,board=15);

      febex1[ 0] = EBYE_FEBEX_HIT(sfp=1,board= 0);
      febex1[ 1] = EBYE_FEBEX_HIT(sfp=1,board= 1);
      febex1[ 2] = EBYE_FEBEX_HIT(sfp=1,board= 2);
      febex1[ 3] = EBYE_FEBEX_HIT(sfp=1,board= 3);
      febex1[ 4] = EBYE_FEBEX_HIT(sfp=1,board= 4);
      febex1[ 5] = EBYE_FEBEX_HIT(sfp=1,board= 5);
      febex1[ 6] = EBYE_FEBEX_HIT(sfp=1,board= 6);
      febex1[ 7] = EBYE_FEBEX_HIT(sfp=1,board= 7);
      febex1[ 8] = EBYE_FEBEX_HIT(sfp=1,board= 8);
      febex1[ 9] = EBYE_FEBEX_HIT(sfp=1,board= 9);
      febex1[10] = EBYE_FEBEX_HIT(sfp=1,board=10);
      febex1[11] = EBYE_FEBEX_HIT(sfp=1,board=11);
      febex1[12] = EBYE_FEBEX_HIT(sfp=1,board=12);
      febex1[13] = EBYE_FEBEX_HIT(sfp=1,board=13);
      febex1[14] = EBYE_FEBEX_HIT(sfp=1,board=14);
      febex1[15] = EBYE_FEBEX_HIT(sfp=1,board=15);

      sync100 = EBYE_SYNC100();

      info_14 = EBYE_INFO_14();
      info_15 = EBYE_INFO_15();

      asic[0] = EBYE_R3B_ASIC_HIT();

      caen[0] = EBYE_CAEN_HIT(module=0);
      caen[1] = EBYE_CAEN_HIT(module=1);
    }
}

EVENT
{
  /* For now, we are eating an entire record at a time. */
  ev = RECORD_EVENT();
}
